import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-others',
    templateUrl: './others.component.html',
})
export class OthersComponent implements OnInit {
    planification: any;
    issues: any;
    constructor() {
      this.issues = [
        {
          "type": "Vehículo",
          "name": "044-OIJGNCS",
          "observation": "Necesita mantenimiento"
        },        
        {
          "type": "Vehículo",
          "name": "784-LKMNBPO",
          "observation": "Necesita mantenimiento"
        },
        {
          "type": "Chofer",
          "name": "PEDRO RODRIGUEZ",
          "observation": "Trabajo 40 horas en la semana"
        },
        {
          "type": "Vehículo",
          "name": "342-GHTYXZA",
          "observation": "Necesita mantenimiento"
        },
        // {
        //   "type": "Vehículo",
        //   "name": "ABCDEFGH",
        //   "observation": "Mantenimiento"
        // },
        // {
        //   "type": "Vehículo",
        //   "name": "ABCDEFGH",
        //   "observation": "Mantenimiento"
        // }
      ]
      this.planification = [
        {
          "ruta": "Caracas",
          "prioridad": 1,
          "hora_de_salidas": [
            "09:00 AM",
            "10:30 AM",
            "01:00 AM",
            "03:30 AM",
            "06:00 AM",
            "08:00 AM",
          ],
          "vehiculo":[
            "007-AXB36ED",
            "102-XXD54FF",
            "038-YP32NBA",
            "205-BBCC55E",
            "001-AAAAXXX",
            "425-RRRRRRR",
          ],
          "chofer": [
            "LORENZO TAMARIT BARRADO - 30",
            "FRANCISCO SIERRA SALES - 32",
            "MOHAMED ROMAN EDO - 45",
            "NICOLAS PONS PUEBLA - 36",
            "JUAN JOSE GONZALEZ - 32",
            "JOSE BELLIDO DEL TORO - 33",
          ]
        },
        {
          "ruta": "Puerto Ordaz",
          "prioridad": 1,
          "hora_de_salidas": [
            "09:00 AM",
            "10:30 AM",
            "01:00 AM",
            "03:30 AM",
            "06:00 AM",
            "08:00 AM",
          ],
          "vehiculo":[
            "222-UDLPRTF",
            "352-DODDY8R",
            "004-M010VW7",
            "032-773ZAMI",
            "098-AKGHTYS",
            "172-EM2MTVT",
          ],
          "chofer": [
            "ALBERTO AYUSO SALGUERO - 40",
            "FELIZ LOPEZ CASCALES - 37",
            "MARCOS FERRERA - 25",
            "ANTONIO BARRERO GALVAN - 29",
            "JOSE RAUL EVOLE - 28",
            "MARTIN VICENS ESTEVEZ - 30",
          ]
        },
        {
          "ruta": "Puerto la Cruz",
          "prioridad": 2,
          "hora_de_salidas": [
            "09:00 AM",
            "11:00 AM",
            "02:00 AM",
            "05:00 AM",
          ],
          "vehiculo":[
            "452-F3R3BVH",
            "325-LOKJGLD",
            "009-P5XZM01",
            "011-WMTU2RT",
          ],
          "chofer": [
            "JORDI POLO VIVO - 33",
            "PABLO MORGADE - 30",
            "NICOLAS MOYANO VEGA - 38",
            "JOSE ANGEL JONES - 34",
          ]
        },
        {
          "ruta": "El Tigre",
          "prioridad": 3,
          "hora_de_salidas": [
            "08:00 AM",
            "02:00 PM",
          ],
          "vehiculo":[
            "111-EI2QRCG",
            "126-LBM28QK",
          ],
          "chofer": [
            "PABLO ACEDO LEDO - 43",
            "MIGUEL GONZALEZ - 29",
          ]
        },
        {
          "ruta": "Delta Amacuro",
          "prioridad": 3,
          "hora_de_salidas": [
            "08:00 AM",
            "02:00 PM",
          ],
          "vehiculo":[
            "708-WQZPDVN",
            "323-DKKKO18",
          ],
          "chofer": [
            "MIGUEL GORDO - 31",
            "ROMMEL GUEVARA - 51",
          ]
        }
      ];
      // console.log(this.planification);
    }

    ngOnInit() {

    }
}
