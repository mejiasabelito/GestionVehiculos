/**
 * Created by andrew.yang on 5/18/2017.
 */
import {Router} from "@angular/router";
import {OnInit, Component} from "@angular/core";

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
    // infoHome: any;
    rutas: any;
    constructor(private route: Router ) {
      // this.infoHome = [
      //   {
      //     "title":"Salidas Registradas",
      //     "nav":,
      //     "number":,
      //     "percent",
      //     "total":
      //   },
      // ]
    }

    ngOnInit() {
      this.rutas = [
        "Caracas",
        "Puerto Ordaz",
        "Puerto la Cruz",
        "El Tigre",
        "Delta Amacuro"
      ];
      console.log("Hola");
    }
    goToPlan(){
      this.route.navigate(['/others']);
    }
    sendData(){
      alert("Data enviada satisfactoriamente");
    }
}
